import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { AnalyticsService } from 'src/app/services/analytics/analytics.service';
import { LanguageService } from 'src/app/services/language/language.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.scss'
})
export class ProjectsComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    navSpeed: 700,
    items: 1,
    autoplay: true,
    autoplayTimeout: 3000
  }
  FeatureProjects: any[] = []
  @ViewChild('imgContainer') imgContainer!: ElementRef;
  slide: string = 'slide';

  constructor(
    public analyticsService: AnalyticsService,
    private languageService: LanguageService

  ) { }



  ngOnInit(): void {
    this.languageService.translateService.get("FeatureProjects.Projects").subscribe(val => {
      this.FeatureProjects = val
    })
  }
  debug() {

    this.imgContainer.nativeElement.scroll({
      top: this.imgContainer.nativeElement.scrollHeight,
      left: 0,
      behavior: 'smooth',
    });
  }

}
