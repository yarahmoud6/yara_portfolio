import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'src/app/services/analytics/analytics.service';
import { LanguageService } from 'src/app/services/language/language.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  AboutMeDAta: any[] = []
  constructor(
    public analyticsService: AnalyticsService,
    private languageService: LanguageService

  ) { }



  ngOnInit(): void {
    this.languageService.translateService.get("AboutMe.Paragraphs").subscribe(val => {
      this.AboutMeDAta = val
    })
  }

}
