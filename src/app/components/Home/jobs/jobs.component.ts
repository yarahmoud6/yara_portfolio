import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'src/app/services/analytics/analytics.service';
import { LanguageService } from 'src/app/services/language/language.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  Jobs: JOBS[] = [];
  active = 0

  constructor(
    public analyticsService: AnalyticsService,
    private languageService: LanguageService
  ) { }

  ngOnInit(): void {
    this.languageService.translateService.get("Experience.Jobs").subscribe(val => {
      this.Jobs = val
    })
  }

}
interface JOBS {
  Tab: string,
  Title: string,
  Description: any,
  Date: string
}