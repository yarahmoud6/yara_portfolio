import { Component } from '@angular/core';
import { AnalyticsService } from 'src/app/services/analytics/analytics.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  constructor(
    private analyticsService: AnalyticsService,
  ) {
  }

  ngOnInit(): void {
    this.analyticsService.sendAnalyticPageView("/yara", "Se entro a yara")
  }


}
