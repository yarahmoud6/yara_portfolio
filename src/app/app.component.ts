import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import * as AOS from 'aos';

import { HomeModule } from './components/Home/home.module';
import { GeneralModule } from './components/general/general.module';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { HttpClientModule } from '@angular/common/http'
import 'css-doodle';
import { TranslateService } from '@ngx-translate/core';
import { Title, Meta } from '@angular/platform-browser';
import { LanguageService } from './services/language/language.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  standalone: true,
  providers: [TranslateService],
  imports: [RouterOutlet,
    NgbModule,
    HomeModule,
    GeneralModule,
    HttpClientModule,
  ],

  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'yara_portfolio';

  constructor(
    private titleService: Title,
    private metaService: Meta,
    private languageService: LanguageService
  ) {
  }
  ngOnInit(): void {

    this.languageService.initLanguage()

    this.titleService.setTitle("Yara Hmoud | Frontend Developer | Angular Development | Software Engineer");
    this.metaService.addTags([
      { name: 'keywords', content: 'Frontend, Angular Developer , React Development , Software Engineer, software, developer' },
      { name: 'description', content: 'As a software engineer with 4+ years of hands-on experience in developing scalable web applications using a wide range of frontend and backend technologies like React, Angular, Laravel and Cakephp.' },
    ]);

    AOS.init();


  }
}
