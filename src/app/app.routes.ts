import { Routes } from '@angular/router';
import { HomeComponent } from './components/Home/home.component';

export const routes: Routes = [

    { path: '', component: HomeComponent },
    { path: '**', pathMatch: 'full', redirectTo: '/' },

];
